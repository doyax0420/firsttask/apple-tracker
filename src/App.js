import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import './App.css';

function App() {

  const [data, setData] = useState([])
  const [btc, setBtc]= useState([])

  function getData(){
    fetch('https://sandbox.iexapis.com/stable/stock/aapl/quote/iexRealtimePrice?token=Tsk_8cf45973dd0c45a889ca533564097bbc')
    .then(res => res.json())
    .then(data => {
        console.log(data)
        setData(data)
    })

    fetch('https://sandbox.iexapis.com/stable/crypto/btcusdt/quote?token=Tsk_8cf45973dd0c45a889ca533564097bbc')
    .then(res => res.json())
    .then(data => {
      console.log(data)
      setBtc(data)
    })
  } 

  setInterval(getData, 10000)
  return (
    <React.Fragment>
        <Container className="bg-secondary text-center">
            <Row>
                <Col md={12}>
                    <div className="box">
                          <h1>Apple Company</h1>
                          <h3>Latest Price Qoute: <p className="text1">$ {data}</p></h3>                        
                          <h3>BTCUSD price: <p className="text1">$ {btc.latestPrice}</p></h3>                        
                          <h3>Apple Latest Price Qoute Converted to BTCUSD: <p className="text1">$ {data * btc.latestPrice}</p></h3>                        
                    </div>
                </Col>
            </Row>
        </Container>
    </React.Fragment>
  );
}

export default App;
